import logo from './logo.svg';
import './App.css';
import Post from './Post';
import People from './people';

function App() {
  return (
    <div className="App">
      <Post />
      <br />
      <People />
    </div>
  );
}

export default App;
