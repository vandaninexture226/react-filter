import React, { useState, useEffect} from "react";
import axios from 'axios';
import { Card, Input } from 'semantic-ui-react';

export default function Post() {
    const [APIData, setAPIData] = useState([]);
    const [searchInput, setSearchInput] = useState('');
    const [filteredResults, setFilteredResults] = useState([]);

    const searchItems = (searchValue) => {
        setSearchInput(searchValue)
        if(searchInput !== ''){
            const filteredData = APIData.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            })
            setFilteredResults(filteredData)
        }
        else{
            setFilteredResults(APIData)
        }
        
    }

    useEffect(() => {
        axios.get(`https://623c15542e056d1037f9919b.mockapi.io/api/v1/users`).then((response) => {
            setAPIData(response.data);
        })
    }, [])

    return (
        <div style={{ padding: 20 }}>
            <Input 
                icon='search'
                placeholder='Search...'
                onChange={(e) => searchItems(e.target.value)} />
            <Card.Group itemsPerRow={3} style={{ marginTop: 20 }}>
                {searchInput.length > 1 ? (
                    filteredResults.map((item) => {
                    return (
                        <Card>
                            <Card.Content>
                                <Card.Header>{item.firstName}</Card.Header>
                                <Card.Description>
                                  {item.lastName}
                                </Card.Description>
                            </Card.Content>
                        </Card>
                        )
                    }) 
                ) : (
                    APIData.map((item) => {
                        return (
                            <Card>
                                <Card.Content>
                                    <Card.Header>{item.firstName}</Card.Header>
                                    <Card.Description>
                                        {item.lastName}
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                        )
                    })
                )}
            </Card.Group>
        </div>
    )
}
