import React, { useState } from "react";

const People = (props) => {
    const people = [
        {name: "Vandan Makwana", number: "8866224118", id: 1},
        {name: "Jay Chauhan", number: "9645854787", id: 2},
        {name: "Jaypal Solanki", number: "7945856574", id: 3},
        {name: "Uday Kantariya", number: "7854145898", id: 4},
        {name: "Siddharth Dave", number: "8954568785", id: 5},
    ];

    const [search, setNewSearch] = useState("");

    const handleSearchChange = (e) => {
        setNewSearch(e.target.value);
    };

    const filtered = !search 
        ? people 
        : people.filter((person) =>
            person.name.toLowerCase().includes(search.toLowerCase())
        );

    return (
        <>
            <h2>Phone Book</h2>
            Filter Person: {" "}
            <input type="text"
                value={search}
                onChange={handleSearchChange} />
            <h2>Numbers</h2>
            {filtered.map((person) => {
                return (
                    <p key={person.id}>
                        {person.name} - {person.number}
                    </p>
                );
            })}
        </>
    );
};

export default People;